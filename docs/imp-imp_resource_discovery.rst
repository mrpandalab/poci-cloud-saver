1. OCI Resource Discovery
=========================

Discovering what OCI Resources ('Resources') can be subject to scheduling is an important start to the implementation process.


Key Question
   Do you wish Pebble IT to discover the resources or are you able to perform the discovery yourself ?

   a) Pebble IT prepares a spreadsheet of Resources
   b) The Organization prepares a spreadsheet of Resources
   c) No Resource discovery is required. Once POCI is operational, the organization will tag the Resources as required.

   If Pebble IT are to perform the Resource discovery, then a Free Tier Linux server is to be created to allow Pebble IT to perform the extraction of resource details so that this may be presented back to you.

   If you wish to perform the Resource discovery, then please prepare a spreadsheet that contains the following columns:

   * Region (can be ommitted if a single region)
   * Compartment (can be ommitted if there is to be no Compartment based scheduling)
   * Resource Display Name
   * Resource Type
   * Scheduling Requirements. This is a freetext field where you describe what scheduling requirements you have, Pebble IT can convert this to formal tag values

   This decision and any subsequent actions are to be performed before the next step.


Scheduling Workshop
   This occurs virtually and its intention is to get an understanding of what Resources are to be scheduled via POCI. The agenda of the workshop will be dictated by the prior key decision regarding Resource discovery.

   Ideally, the following will be understood as a result of the workshop:

   * What Resource Types are subject to scheduling
   * What Usage Designation should be subject to scheduling - Production, Training, QA/Pre-Prod, Testing, Development and any others
   * Use of Compartment-Level scheduling v Resource scheduling
   * Any particular requirements that will need to be considered
   * Likely schedules for different Resources

