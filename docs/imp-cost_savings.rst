Calculating OCI Cost Savings
============================

Cost Savings calculations are normally performed by Pebble IT and use the following information:

* Your OCI Rate Card if you have negotiated discounts from Oracle
* Your monthly/quarterly/annual commitments agreed with Oracle
* The Resources and their intended usage profile.

This information is normally gathered during workshops conducted with key stakeholders and findings are then presented back in subsequent meetings. The amount of cost reduction will be determined by the number of resources that are subject to scheduling and how many hours the usage of the Resources can be reduced to.

Note that your commitment with Oracle is important, as POCI cannot lower your costs below the minimum spend agreed with Oracle. Pebble IT works with clients on optimizing the procurement arrangements with Oracle for their OCI usage, but unfortunately there are cases where clients have committed 100% of their expected usage with Oracle as annual spend -- therefore removing any ability to reduce the costs paid to Oracle. However, whilst the monetary amount may not be reduced, POCI can decrease the cost incurred so that additional resources can be increased up to the minimum spend amount.

As an example, if you have committed to spend US$300,000 with Oracle on a 3-Year commitment with monthly 'overage' invoicing, then any usage less than $100,000 per year will still result in the minimum of $100,000 being paid to Oracle (if it hasn't been paid already which is normally the case). Let's demonstrate on how POCI can be used to your advantage in this example:

* Your minimum spend with Oracle --> $100,000 per year
* Your current usage on OCI --> $10,000 per month ($120,000 per year)
* It is calculated that POCI can reduce your POCI spend by 40% -- therefore the $120,000 can be reduced to $72,000
* However, the minimum spend is $100,000 -- therefore this represents a 'waste' of $28,000
* The client then migrates their Production Databases to multiple-node RAC Extreme Performance databases to obtain a better outcome at an additional cost of $30,000 per annum
* The client now has an estimated total usage cost of $102,000 per year -- reduced from $120,000 and a significantly better business outcome of increased availability and performance within the Production database.

In the example above, another alternative could be the client migrating additional workloads onto OCI with no additional spend until spend passed the minimum of $100,000. Therefore, there is still ample opportunity for POCI to add value.

The role of the implementation is to understand how to maximise the savings that POCI can deliver, and Pebble IT will bring their experiences to the table to give ideas (like in the example above) so that all potential benefits can be explored.
