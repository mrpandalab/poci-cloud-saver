The POCI Server
===============


The POCI Server is a Virtual Machine (VM) that is an integral part of the Service of POCI, and is to be managed by Pebble IT even though it is recognised that the VM is ultimately the responsibility of the organization.

Contact Pebble IT via POCI Support for any matters of:

* Changing of any details of the Server including its name, VCN & Compartment
* Whether any Management Agent changes need to be implemented including the Bastion Service
* Any changes to the POCI Security Policies that are being considered
* Any internal policy changes that may impact the operating system and patching policy of the POCI Server
* Any notice from Oracle that the POCI Server requires a maintenance outage
* Any significant incident has occurred within your Tenancy that may require Pebble IT to take precautions or action


Pebble IT will perform all routine maintenance of the POCI Server as part of the POCI Subscription.

.. tip::

   If the POCI Server is configured to be on Oracle Autonomous Linux, patching is already performed automatically by Oracle and does not need to be managed seperately by Pebble IT.


Pebble IT will also maintain the POCI software on the Server and perform all necessary upgrades. The POCI Server is compatible with Oracle Cloud Guard, the Oracle Cloud Agent, In-Transit Encryption and the OS Management Service.


.. seealso::

   Refer to the :doc:`Support <ug-support>` section for additional details on obtaining support and assistance for POCI.

