#########################
POCI Implementation Guide
#########################

The Implementation Guide is a document to be used by Cloud Administrators to understand how POCI would operate at your organization and what options can be made to meet your requirements.

   **Implementation Guide Contents**

.. toctree::
   :maxdepth: 2

   imp-how_poci_works
   imp-cost_savings
   imp-implement

