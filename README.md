# POCI Cloud Saver

Schedule OCI Resources. Refer to (https://pebbleit.com.au/poci). 


## Support
If you require any support for POCI, please email us at poci@pebbleit.com.au


## Contributing
We welcome any ideas or enhancements for POCI that would be of benefit to yourself or other clients. Please email us in this instance. There are no updates or pull requests allowed for people outside of the development team.


## License
This documentation is licensed GPL v3 and is included in this repository. Copyright (C) 2022 Pebble IT Solutions Pty Ltd. <https://pebbleit.com.au/>


## Project status
Currently at Version 2.0 with some improvement being worked on for incremental releases. We expect major releases (Version 3.0, 4.0, etc...) each year.


## POCI 1.x users
Your POCI installation will be automatically upgraded to POCI 2.x and this will have been communicated to your management. You will know that you are on POCI 2.x by the format of the schedule tag values being updated to the POCI 2 Format.
