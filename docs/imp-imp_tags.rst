2. Tag Design
=============

This Implementation step can be performed by completing a worksheet supplied by Pebble IT. A workshop to facilitate this is at the discretion of the organization - however, it will be required if English is not to be the language used for Tags. It will address the following:

Language requirements
    Confirming that English is the required language, and if not English, what english-based values will be used


Week definition
    What days are considered a 'Weekday', and the remainder 'Weekend' - and what terms to use for Weekday/Weekend


Days of the week
    What name should be used for each Day. Even if English, it can be decided to use the fullname (eg Monday), shortname (eg Mon) or abbreviation (eg Mo).


Tag actions
    A number of actions form part of the Tag Names, these names can be customised to specific values required by the organization or the default values can be used.


