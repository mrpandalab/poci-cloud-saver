7. POCI Server Design
=====================

POCI runs on an Oracle Linux Server (VM) within your Tenancy. The implementation captures the OCI Instance details of the POCI Server so that it may be created by either Pebble IT or the organization. All of the fields can be set according to the organization's requirements:

   * Name: This could be 'POCI SCHEDULING SERVER'
   * Compartment: By default it would reside within the 'POCI SCHEDULING' compartment.
   * Availability Domain & Fault Domain
   * The VCN that it is to operate within - this could be a new VCN or associated within an new VCN specific to POCI
   * Operating System: Either Oracle Linux 7 (with or without Autonomous Linux) or Oracle Linux 8
   * Instance Shape: POCI can operate on a Free Tier Intel Instance (VM.Standard.E2.1.Micro) or a Free Tier Ampere Instance (VM.Standard.A1.Flex). The Shape chosen may also determine what Operating System is selected (for example, Autonomous Linux is not available on Ampere at this point in time). POCI only requires 1 OCPU and 6GB RAM to operate unless it is working with a significant number of resources to schedule.


As POCI is maintained by Pebble IT, it cannot be run from a shared instance that contains other workloads, it must be dedicated to POCI. The POCI Server must operate at all times and cannot be part of any routine downtime. The POCI Server is to be treated as a Production Instance and is part of the Managed Service provided by Pebble IT for the operation of POCI.
