3. Configurable Options
=======================

During the implementation, the below options are presented, all of which have default values, and the organization is given the opportunity to discuss the implications of the option and to alter its default setting.


+------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Option                       | Details                                                                                                                             |
|                              |                                                                                                                                     |
+==============================+=====================================================================================================================================+
| Backup Check Times           | Times to check for backup completion. See details below                                                                             |
+------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Freeform Schedule Tag        | Default value 'POCI SCHEDULING'. This is what is referenced on the Freeform Resource Tag to display updates performed by POCI       |
+------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Log Retention                | the POCI Server retains logs for 180 days by default and the OCI Logs can be retained 1 to 6 months (2 months recommended)          |
+------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Maintenance Day & Time       | The day and time to perform security updates to the POCI Server. Recommend to occur on your Sunday 12:05am                          |
+------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| MySQL Server Stop Type       | Default is 'SLOW'. Can be 'FAST' or 'IMMEDIATE'                                                                                     |
+------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Regions to incl/exclude      | By default all regions that you are subscribed to are included, some can be excluded. The order they are processed in can be set    |
+------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Tag/Region timezones         | A timezone is defaulted per Region, this can be changed and impacts what timezones are used by Admin in entering Scheduling details |
+------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Compartment Tag Behaviour    | The default is to use Compartment Tags if the Resource Tag is empty - otherwise override Resource Tag with Compartment Tag          |
+------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Compartments to incl/exclude | Compartments can be nominated to be excluded or included. The default is to allow all compartments                                  |
+------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Resource Types to include    | Resource Types can be nominated to be included and therefore all others excluded. The default is to allow all Resource Types        |
+------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+


Backup Check Times
   Default value = [35, 40, 50, 55]. Other allowed values are any 5 minute interval from 0-55 and 1 value must be provided.

   Performing queries to determine if a backup is complete for a resource is an expensive operation. If there are a significant number of resources (50+) with a backup check, then this could add significant processing time to each POCI run, therefore these checks are only performed at nominated times -- ideally when little or no other scheduling events occur.

   As OCI charges for a resource that is running at anytime within the UTC hour, there is little reason to stopping a Resource at the start of the hour, it is preferable to stop the Resource towards the end of the hour.

   If it is anticipated that one of the default values will be used for starting or stopping a large number of Resources, then that value should be removed from the check times and potentially substituted with another suitable value that will not be as popular on tags as a start or stop time.

