##############
How POCI Works
##############

The diagram below illustrates the primary concepts explaining how POCI works in your OCI Tenancy.

.. image:: how_poci_works.png
   :width: 70%
   :align: center
   :alt: How POCI Works illustration



Key Components
==============

POCI Server
   This is an Oracle Linux instance that hosts the POCI software. It is not public facing minimising the potential of being a source of external security compromise. It contains identity details that are used to allow it to perform authorised queries and requests with the OCI Cloud Control Plane. POCI checks in 5 minute intervals if there are any any scheduling actions to occur or any notifications to send to Administrators. Each implementation of POCI has its own configuration that is tailored for the OCI Tenancy and this determines what work it performs and whether particular options are enabled and restrictions are imposed.


OCI Cloud Control Plane 
   All POCI requests are made to the OCI Cloud Control Plane using an authenticated request and information is provided by OCI back to POCI that is then used to determine if any scheduling actions are required. The Control Plane ensures that all work performed by POCI adheres to the Security Policies of your Tenancy and that only authorized identities are permitted to perform that work.


Regions
   The Tenancy subscription will determine what OCI Cloud Regions are available, and configuration within POCI itself can determine what Regions are eligible for scheduling, this is how POCI can also perform scheduling on Regions that are not part of the public OCI Cloud Backbone including Gov Regions, Dedicated Regions and Cloud @ Customer implementations where the Cloud Control Plane is implemented.


Compartments
   All OCI Resources that can be scheduled by POCI are associated with a Compartment including the Tenancy itself which is the 'Root Compartment'. Compartments are used by POCI as an optional parent for scheduling purposes and can be configured to inherit the tags from the Compartment and/or to override values on the Resource itself. This is useful if there are a number of resources in the Compartment that have the same scheduling requirements -- the Tag details do not need to be repeated on each Resource, just defined on the Compartment only thereby ensuring consistency and simplicity.


Resources
   Any eligible Resource in your Tenancy can have their Scheduling Tags entered with scheduling details. Each Resource is checked for its current state and scheduling requirements, including overriding any details from the Resource's Compartment. If a scheduling action is to occur, then POCI will update the Resource's Tags with action details and will request OCI to perform the required action. POCI will also update the Resource's Tags into a consistent format that is simple to read yet caters for multiple complex requirements.


OCI Notification Service
   POCI does not send emails itself. All messages are configurable during the implementation and are sent to the OCI Notification Service. You can maintain your subscriptions and destinations for the notifications that are independent to POCI. As POCI is unaware of the notification configuration within OCI, there is no awareness of how many subscribers or what the destination of the notification is, that is all addressed by OCI itself and can be maintained independent of POCI. Notifications concern scheduling events that have been performed and problems encountered by POCI including events like badly formatted tags or unexpected errors.

   Some Notifications for scheduling events can be configured to be only sent periodically. For example, a scheduling event can be configured to be performed immediately or alternatively, all scheduling events grouped into a single notification 8am daily -- in this case it would include all scheduling events that have not yet been notified. In this example, you would simplify the notifications to the interested parties who could review in a single notification what scheduling occurred (resources stopped and started) to ensure that all events are occurring as expected rather than reviewing a large number of individual notifications which would be unwieldly and inefficient.


OCI Logging Service
   POCI emits key information during its execution. There are 2 logs maintained by POCI; Scheduling Events, and Errors. Whilst logs are maintained on the POCI Server for a period of 6 months, the individual log entries are passed to the OCI Logging Service within your Tenancy allowing Admins to review the logs online. This will enable Admins to review past scheduling events (rather than relying on finding older notifications that are most likely deleted) as well as retrieve the details of any errors being encountered for support purposes. This also ensures that the internal Admin team does not need to access the POCI Server to retrieve any details, it is available through the OCI Control Panel and access can be controlled through IAM Policies.


Your Tenancy
   POCI operates within your Tenancy, and therefore is secured to your Tenancy and ensures it is not a security risk due to the sensitive nature of its operations. Your Tenancy consists of a number of Regions that can be subscribed to and can also connect to client-specific Regions for other Oracle Cloud Infrastructure used by clients.
