#################
Implementing POCI
#################

`Pebble IT <https://www.pebbleit.com.au>`_ will lead you through a number of workshops that facilitates the necessary decisions required to allow POCI to be implemented. The implementation tasks are quick to perform and do not require a significant amount of time on your Cloud Administrators. If sufficiently qualified personnel that are informed of your OCI Tenancy and the goals to be achieved by POCI, then the Implementation effort required by your organization is small and efficient.

This document is not used for implementation, more detailed materials are used during the process, this document describes the purpose and information gathered at each step.


.. toctree:: Implementation Steps
   :maxdepth: 0

   imp-imp_resource_discovery
   imp-imp_tags
   imp-imp_options
   imp-imp_user
   imp-imp_security
   imp-imp_notifications
   imp-imp_server


**Implementation Steps**

Implementing POCI is relatively straight-forward, however to ensure that the maximum benefit is obtained from POCI we recommend working through a series of steps that results in POCI being optimised for your site and operational ready to use. These steps are:


.. image:: imp-steps.png
   :width: 70%
   :align: center
   :alt: POCI Implementation Steps


Many of the workshops outlined in the steps above can be combined into a single workshop that is facilitated by Pebble IT. It is possible that the time from starting the POCI implementation to completion is as little as one week.
