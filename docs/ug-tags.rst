OCI Resource Tags for POCI
==========================

Entering Scheduling Tags
------------------------

POCI Schedule Tags are 'OCI Defined Tags' with a Namespace of "Schedule" (unless the namespace has been altered as part of your implementation). The Schedule Tag Namespace has a list of different schedule tag 'Keys' for the user that we refer to as 'Tag Types'. The image below illustrates the default Key values for schedule tags -

.. image:: tags-entry.png
   :width: 95 %
   :align: center
   :alt: Entering POCI Schedule Tags

What is entered into the Schedule Tags is determined by the 'Key' chosen by the user. Further details are on that below. Multiple Schedule Tags can be entered for a Resource, but the Key must be unique for the Schedule Tag Namespace and Resource combination.

After a tag is entered and saved, it will appear in a readable format that can be edited for further adjustments if required. Below is an example of a Schedule Tag set for 'Everyday'.

.. image:: tags-entered.png
   :width: 95 %
   :align: center
   :alt: Entered POCI Schedule Tags


Tags are Everywhere!
~~~~~~~~~~~~~~~~~~~~

Tag Namespaces (for example, 'Schedule' Tag Namespace for POCI) will be available to select on any taggable resource within your OCI Tenancy in all subscribed Regions. The assignment of a Schedule Tag does not mean that it will be scheduled by POCI. For example, tags can be entered on a 'Public IP Pool' within OCI, this does not mean that it can be started and stopped by POCI. OCI does not permit configuration of what Resource Types are eligible for Tag Namespaces, all Tag Namespaces are available on all Resource Types.


Tag Keys
~~~~~~~~

As shown in the image above, there are multiple Tag Keys for the user to select from. These are:

* Monday - Sunday: The day of the week. The current time is used to determine what the current day of the week it is and if this tag is present, then it is the source of schedules
* Weekdays: This is normally defined as Monday to Friday but can be configured within POCI. Use Weekdays when the same schedule applies to each day of the weekday
* Weekend: This is normally defined as Saturday to Sunday but can be configured within POCI. Use Weekend when the same schedule applies to each day of the weekend
* Everyday: This is every day of the week. If you have a consistent schedule then a single entry for Everyday will apply to all days of the week
* IgnoreUntil: POCI allows you to enter a date and time where the scheduling operations can be ignored until.
* NextSchedule-Start/Stop: Rather than ignoring all scheduling, POCI can also be instructed to not allow any Start or Stop operations until the specified date and time.
* Special: The 'Special' tag is not used for entering scheduling information, but for more advanced use with configuration overrides

Multiple Schedule Tags can be entered for a Resource, but the Key must be unique for the Schedule Tag Namespace and Resource combination.


Tag Hierarchy
-------------

Scheduling tags are examined in the following order:

   1. Ignore Tags
   2. Today Tags. For example, if today is Monday, then the Monday tag will be examined, the Tuesday, Wednesday,... tags are not examined
   3. Weekday/Weekend Tags. If today is a Monday and this is classified as a 'Weekday', then it will be examined, whereas in this case, the 'Weekend' tag will not be examined. If Saturday is defined as a 'Weekend' tag, then it will be examined and 'Weekday' not examined.
   4. Everyday Tag. This is the final tag that is examined after all others.

POCI will search through all the tags and once it finds the first eligible Tag within the hierarchy, it will use that Tag and no other tags are considered. For example, if the current day is Monday, and the following Schedule Tags have been defined:

   * Tuesday - ``06:30 - 22:50``
   * Weekday - ``08:45 - 21:30``
   * Weekend - ``10:00 - 14:30``

POCI will initially search for a 'Monday' tag - but as none is found, it will then look for the next level in the hierarchy which is 'Weekday'. The Tuesday and Weekend tags will be ignored because the current day is Monday. Therefore the Resource will start at 8:45 am and stop at 9:30 pm.

Staying with the same example above, if today is Tuesday, then POCI will use the 'Tuesday' tag and will not use the 'Weekday' tag as its search has already been satisfied by the higher priority day tag. This will result in the resource starting at 6:30 am and stopping at 10:50 pm -- the Weekday scheduling of 8:45am/9:30pm is never considered.

In the example above, it would be pointless to have an 'Everyday' tag as all days are covered by the 'Weekdays' and 'Weekend' tags and therefore would never be considered.


Time Intervals
--------------
Schedule times are at any time represented as 24 hours, with 00:00 being midnight and the first time of the day, and the last time is 23:55. All times are entered in intervals of 5 minutes. If a time is entered that ends in 1, 2, 3, or 4 then it is treated as '0' and any number greater than 5 is treated as '5'. For example, a time entry of ``12:06`` is treated as ``12:05`` and ``16:02`` is treated as ``16:00``.

Note that OCI bills by the UTC Hour, therefore the first second of eligibility of an hour is '00' minutes. If you start a Resource at '00', its OCI billing will start for that UTC Hour.


Timezones
---------

What timezone the times are entered into is part of the implementation of POCI. By default, POCI implements a localised timezone per OCI Region, therefore Cloud Administrators should be maintaining schedule tags in the regions's local timezone. Alternatively, a single timezone could be set for all regions (e.g. 'UTC').

The local timezones that are the default per OCI Region are --

+------------------+--------------------+
| OCI Region       | Timezone           |
|                  |                    |
+==================+====================+
| ap-chuncheon-1   | Asia/Seoul         |
+------------------+--------------------+
| ap-hyderabad-1   | Asia/Calcutta      |
+------------------+--------------------+
| ap-melbourne-1   | Australia/Melbourne|
+------------------+--------------------+
| ap-mumbai-1      | Asia/Calcutta      |
+------------------+--------------------+
| ap-osaka-1       | Asia/Tokyo         |
+------------------+--------------------+
| ap-seoul-1       | Asia/Seoul         |
+------------------+--------------------+
| ap-singapore-1   | Asia/Singapore     |
+------------------+--------------------+
| ap-sydney-1      | Australia/Sydney   |
+------------------+--------------------+
| ap-tokyo-1       | Asia/Tokyo         |
+------------------+--------------------+
| ca-montreal-1    | America/Toronto    |
+------------------+--------------------+
| ca-toronto-1     | America/Toronto    |
+------------------+--------------------+
| eu-amsterdam-1   | Europe/Amsterdam   |
+------------------+--------------------+
| eu-frankfurt-1   | Europe/Berlin      |
+------------------+--------------------+
| eu-marseille-1   | Europe/Paris       |
+------------------+--------------------+
| eu-zurich-1      | Europe/Zurich      |
+------------------+--------------------+
| il-jerusalem-1   | Asia/Jerusalem     |
+------------------+--------------------+
| me-dubai-1       | Asia/Dubai         |
+------------------+--------------------+
| me-jeddah-1      | Asia/Riyadh        |
+------------------+--------------------+
| sa-santiago-1    | America/Santiago   |
+------------------+--------------------+
| sa-saopaulo-1    | America/Sao_Paulo  |
+------------------+--------------------+
| uk-cardiff-1     | Europe/London      |
+------------------+--------------------+
| uk-gov-cardiff-1 | Europe/London      |
+------------------+--------------------+
| uk-london-1      | Europe/London      |
+------------------+--------------------+
| uk-gov-london-1  | Europe/London      |
+------------------+--------------------+
| uk-london-1      | Europe/London      |
+------------------+--------------------+
| us-ashburn-1     | America/New_York   |
+------------------+--------------------+
| us-gov-ashburn-1 | America/New_York   |
+------------------+--------------------+
| us-gov-chicago-1 | America/Chicago    |
+------------------+--------------------+
| us-gov-phoenix-1 | America/Phoenix    |
+------------------+--------------------+
| us-langley-1     | America/New_York   |
+------------------+--------------------+
| us-luke-1        | America/Phoenix    |
+------------------+--------------------+
| us-phoenix-1     | America/Phoenix    |
+------------------+--------------------+
| us-sanjose-1     | America/Los_Angeles|
+------------------+--------------------+

.. note::

   Note that POCI configuration allows the addition of other Regions
   not in the list above and any Timezone from
   https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
   can be used for a list of eligible timezones.


Tag Value Structure
-------------------

There are different tag structures used for the following type of Schedule Tags:

* Day Tags. This includes the tags of Everyday, Weekdays, Weekend and the individual days of the week
* Ignore Tags. This includes the tags of IgnoreUntil, NextSchedule-Start and NextSchedule-Stop
* Special Tags. This is for advanced use only. Refer to the :doc:`Advanced <ug-advanced>` section for further details

The format of the values for each of the above tag types is explained below.


Day Tags
~~~~~~~~

Day Tags include the 'Everyday', 'Weekday', 'Weekend' and 'Monday' to 'Sunday' tags. They are to contain the times to start and stop Resources. The simplest format of the tag is:

   ``starttime - stoptime, starttime - stoptime, ...`` where the times are 24hr time entries that can be formatted as , ``hmm``, ``hhmm`` or ``hh:mm``

Examples are:

* ``700 - 1930`` -- this equates to start at 7:00 am, stop at 7:30 pm. The leading 0 for 7 am is not required as POCI can determine that this is a 24 hour time corresponding to 7:00 am
* ``0700-1930``  -- this equates to start at 7:00 am, stop at 7:30 pm. Note that spacing is not necessary
* ``07:00 - 19:30`` -- this equates to start at 7:00 am, stop at 7:30 pm
* ``700-945, 1000-1300`` -- this equates to start at 7:00 am, stop at 9:45 am, start at 10 am, and stop at 1pm.

Note that there is no use of am/pm, all times are 24hr format only, and there is no limit to how many start/stop combinations can be entered on a single tag value so long as they are seperated by a comma (spacing is not necessary).


**Complex Formats**

When POCI reads a tag in simple format, it will convert it to complex format for you. The complex format is:

   ``action[starttime - stoptime], action[starttime - stoptime], ... # comments``

The use of a comprehensive tag format that is maintained by POCI ensures that there is an easy and efficient way of entering simple schedules, but also a lot of flexibility in catering for comprehensive scheduling requirements. There are 3 actions that can be specified:

* **run**. This is the basic action of specifying when you want a Resource to run and is used like ``run[starttime - stoptime]``
* **until**. This provides the ability to enter a temporary schedule that overrides the normal 'run' schedule. Until requires an end date and optional time to be considered. Its use is ``until_dd-mon-yy hh:mm[starttime - stoptime]``
* **today**. This is a shortcut for 'until' but with today's date. If today's date is 15-Dec-2022, then ``today 22:30[starttime - stoptime]`` is the same as ``until_15-dec-22 22:30[starttime - stoptime]``

Multiple schedules within a single tag are considered **segments**, and each segment is processed from :underline:`right-to-left` by POCI. Once a segment equates to the current time, it is then used by POCI -- this should be considered when entering complex scheduling requirements that contain multiple segments.

The *until* and *today* tag without a time will be considered until the start of that day - 00:00, if it is to be later than that day, then you may specify a time with HH:MM. For example:

   ``run[07:00 - 20:00], until_14-dec-22 23:05[07:00 - 23:00]``

will result in POCI will consider this temporary rule until 11:05pm, after that, the rule will be removed and the tag value will then return to:

   ``run[07:00 - 20:00]``

When the **until** date becomes today, POCI will change the ``until_dd-mon-yy hh:mm`` to ``today hh:mm`` so that it is clear that this rule will end today. If you want a temporary rule to end today, you may enter the value of 'today' rather than the 'until_date...' full value. Remember that you must specify a future time in HH:MM otherwise the rule will be deleted as 'Today' will be treated as 'Today 00:00'.

Comments are entered after a ``#`` character. Any information entered after the # is ignored, therefore prior values can be stored as a comment or an explanation of a complex rule could be entered, or whom to contact to seek further information from.

Some examples of using comments:

* ``run[07:00 - 19:50] # Contact Dave McPherson before you change this``
* ``run[07:00 - 19:50] # old schedule --> run[07:00 - 20:00] -- stop before 8pm so that we pay less!!``
* ``run[07:00 - 19:50] # Ensure the web servers don't start until 15 mins after the start of this database otherwise they will error``


Multiple Until Segments
+++++++++++++++++++++++

It is possible to chain multiple 'until' segments in the one tag value. For example, there might be a permanent tag segment, then a tag setting that is valid for a month, then a tag setting that is valid for a week. As POCI processes tag segments from right-to-left, the earliest 'until' tag segment should be the last segment. Below is an example:

   ``run[07:00 - 19:50], until_03-jan-23[NEVER - 19:50], until_23-dec-22[07:00 - 22:00]``

This example illustrates that until 23rd December, 2022 the Resource will start at 7 am and stop at 10 pm, from then onwards until 3rd January, 2023 the Resource will never start from a schedule and only stop if it is running.

.. warning::

   Be aware that POCI processes the first eligible 'until' date it encounters, hence why the earliest date needs to be last. At this point in time you cannot have multiple schedules on the same day using the 'until' action, this capability may be part of future releases.


Alternate Start & Stop Time Values
+++++++++++++++++++++++++++++++++++

In addition to ``starttime - stoptime`` there are other special values that can be used.

NEVER
   This instructs POCI to never consider either a start or stop of the instance. It can be used as either a start or stop instruction or both. For example, you have some resources that are never to be started on a schedule, but if they are running they are to be stopped at 9:50pm to lower costs, then you would enter this as ``run[NEVER - 21:50]``. A common use case is on 24x7 Production Resources, to ensure that there is no opportunity for mistake, you may consider entering the tag value of ``run[NEVER - NEVER]`` which instructs POCI never to start nor stop these resources. Therefore if an Administrator accidentally goes to the wrong resource for Tag updates, it will be clear to them that they are in the wrong Resource, particularly if combined with a comment like

   ``run[NEVER - NEVER] # Production resource, do not schedule!!!``

   A common use of the ``NEVER`` value is on weekends, where personnel may manually start resources to perform out of hours work, but if they are forgotten to be stopped, then POCI can stop it at a designated time so that the Resource is not left running and incurring unnecessary cost. An example of this tag may be:

   Schedule . **Weekday**: ``run[07:00 - 19:50]``

   Schedule . **Weekend**: ``run[NEVER - 21:50] # Stop on the weekend in case we forget to turn off``

BACKUP
   This special time value can only be used on Database Services and MySQL databases. It instructs POCI to stop the database *after* it detects that a backup has completed for the resource. When you configure automatic backups within OCI, you nominate a time period for the backup to occur which is normally a 2-hour window that the backup commences within. At the conclusion of that backup completing successfully, POCI will stop the resource if requested. Note that if the backup fails, then POCI will not stop the database -- only successful backups are considered. For example, if your database is configured to perform backups daily from 8pm to 10pm and you have a tag of ``run[07:00 - BACKUP]`` then it is possible to see POCI stopping the resource at 8:25pm, 8:40pm, 9:10pm, etc... due to the varying times that the backup completed.

   If you schedule your databases to stop before backup commences or completes, you will see backup failures reported against the database within the OCI Control Panel. Using the BACKUP time variable avoids these issues. As OCI performs a backup every day, but you do not wish to incur the cost of backups on weekends, you could specify the following assuming your backups are configured to commence from 8:00 pm to 10:00 pm daily:

   Schedule . **Weekday**: ``run[07:00 - BACKUP]``

   Schedule . **Weekend**: ``run[NEVER - 19:50] # We stop before backups are scheduled to commence``

   This will result in the Friday night backup occurring, but the next backup will be Monday. However, this is not recommended for Production instances and you should test the recovery of your database if this method is being followed.


Ignore Tags: Ignoring / Pausing Scheduling
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A frequent requirement is to defer/postpone/pause/ignore schedule events due to the Resources being required for one-off purposes. Often this is required due to patching and/or upgrading activities. A common requirement is for a database to be available out of normal hours for patching purposes, if your tags were:

   Schedule . **Weekday**: ``run[07:00 - BACKUP]``

Then you could address this requirement in a number of ways depending upon when you wish to do the tag update. If you wanted to enter this directive up to 6 days ahead, then use of a temporary Day Tag:

*Use of Temporary Day Tag*:
   Schedule . **Weekday**: ``run[07:00 - BACKUP]``

   Schedule . **Friday**: ``Today_23:55[07:00 - NEVER]``

The Friday tag will not be read until Friday, therefore the 'Today' variable will be maintained until the Friday. The Friday tag is considered before the Weekday tag, therefore the NEVER stop instruction is used, not the BACKUP instruction.

If you were performing the tag updates on the Friday itself, you also have the option to alter the Weekday tag like:

*Use of Temporary Weekday Tag*:
   Schedule . **Weekday**: ``run[07:00 - BACKUP], today 23:55[07:00 - NEVER]``

Or you could use an Ignore Tag. There are 3 ignore tag directives to consider:

1. **IgnoreUntil**. This instructs POCI to ignore all schedule tags until the nominated date and time has passed.
2. **NextSchedule-Start**. This instructs POCI to ignore all **start** actions until the nominated date and time has passed.
3. **NextSchedule-Stop**. This instructs POCI to ignore all **stop** actions until the nominated date and time has passed.

To satisfy the above example, we have two alternatives we could implement on the Friday itself after the start action has occurred at 7 am:

a. Schedule . **IgnoreUntil**: ``28-oct-22 06:00`` This instructs POCI to ignore all scheduling requests until the Monday morning at 6 am. Therefore the 'Weekday' instruction of start at 7 am will be eligible to be processed on the Monday.
b. Schedule . **NextSchedule-Stop**: ``27-oct-22 19:00`` This instructs POCI to ignore all stop schedule events until Sunday, 27th October 2022 at 7 pm. Therefore, if the Resource was running, any 'Weekend' instruction to stop after Sunday 7 pm would be eligible to be performed.

There can be multiple ways of achieving required outcomes, there is no particular right or wrong way. Use the method that best articulates what is trying to be achieved.

.. seealso::

   If you wish POCI to ignore all scheduling or all start/stop actions for all resources, then you do not need to update all resources. This capability is explained within the :doc:`Advanced <ug-advanced>` section.


Special Tags
~~~~~~~~~~~~

Special Tags are used to set parameters and enable specialised behaviours on POCI. It is not used for normal scheduling operations. This is addressed in the :doc:`Advanced <ug-advanced>` section.


Compartment Tags
----------------

Tags may also be entered against the Compartment of Resources. Only the immediate (Child) Compartment is considered, not the Parent Compartment. For example, if a Resource is within the ``Production/databases`` compartment, then only tags entered in the ``databases`` compartment will be considered for Resources within that compartment.

Note that the Tenancy is the ``root`` compartment, and this can also be tagged for POCI Scheduling.

There is no difference in entering tags for a compartment. However, there are differences in how compartment tags are considered. When implementing POCI, it may be decided to:

* Consider the Resource Tags before considering the Compartment Tags. This is the default behaviour of POCI.
* Consider the Compartment Tags before considering the Resource Tags. If this configuration is implemented, then only if there are no tags for the compartment will the Resource tags be considered.

In both cases, the Ignore Tags of the Resource are always considered, therefore if you wish to postpone scheduling for a single resource within a compartment that is scheduled by compartment tags, you can enter Ignore tags for the resource and it will be honoured by POCI. This is an important consideration as temporary Schedule Day tags on the Resource will not be considered if POCI is configured to consider compartment tags first.


DBSystem Tags v Database Tags
-----------------------------

As all Resources within OCI may be tagged, there is the opportunity to enter tags for parent Resource entities and child Resource entities. One such example is the 'DB System' Resource that has child 'Database' Resources. POCI allows the entry of Tags for both the child 'Database' Resource and the parent 'DB System' Resource. POCI will first check for the tags against the Database and if not found, will then check the DB System. If both Database and DB System have tag values, then only the Database tags will be considered.

This can be a cause of confusion, therefore it is recommended to agree the standard to comply to within your organization. If you choose to implement tags at the Database level, you may enter tags at the DB System level with comments to state to only enter tags at the Database level -- this is acceptable because the DB System tags will never be read, but the alternative of putting commented tags on the Database to tag at the DB System level will result in the Database tags being used and scheduling not occuring. Therefore, with DB Systems and Databases, always check the tags at DB System first before checking the Database tags.

.. tip::

   You can enter a Schedule.Special tag with a comment only to convey information to other parties about Scheduling


MySQL Database Tags
-------------------

Some resources within OCI can only have tags entered or updated when they are running. MySQL databases are one such Resource that this applies to. This can present a difficult situation when you wish to enter new tags or update existing tags but the database is currently stopped and it is not an option to start the database and wait for it to be running to perform the update, then stop it.

POCI caters for this anomoly by allowing Resource tags to be entered against the 'Special' tag definition on the POCI Server. You will need to copy the OCID of the MySQL Database and perform a special entry on the POCI Server "Schedule.Special" tag -- refer to the :doc:`Advanced <ug-advanced>` section for additional details. Note that this can be done for any Resource, not just MySQL Databases and is an excellent method for performing mass tag updates without having to query and update individual Resources.


Bad Tag Formats
---------------
If a tag **segment** is not in an understandable format, then POCI will cease to attempt scheduling of that Resource and report a 'Bad Tag' error that will appear in the POCI Warning Logs (available in the OCI Logging Service) as well as send a notification periodically to inform Administrators to update this tag.

Once updated and correctly formatted, POCI will perform scheduling as expected. As POCI reads the tag **segments** from right-to-left, if the first eligible segment is correctly formatted, the scheduling will still work, the 'Bad Tag' error will only be encountered when POCI attempts to read that segment, therefore if the left-most segment is badly formatted, the error may not be reported for some time depending upon when it is first required.

