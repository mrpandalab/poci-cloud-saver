POCI Notifications
==================

About POCI Notifications
------------------------

Notification formatting and frequency is designed as part of the Implementation. Further information can be view at the :doc:`Implement Notifications <imp-imp_notifications>` section.

The following notifications are sent by POCI:

* Scheduling Events. This is the starting and stopping of OCI Resources
* POCI Warnings. This sends information on Resources that have incorrectly formatted Tags and updates to Tenancy Tags required
* POCI Errors. This sends information on errors encountered by POCI.

Whilst POCI determines the content and layout of the notifications, it is OCI that controls who receives these notifications. This is done by subscribing to the Notification Topic as shown below:

.. image:: topic-subscribe.png
   :width: 95 %
   :alt: Subscribing to an OCI Notification Topic
   :align: center

If it is an email subscription, then OCI will send a confirmation email and POCI notifications will not be received by the recipient until the confirmation email has been acknowledged. Whether there is no subscribers or thousands of subscribers is independent of POCI and is performed solely within OCI.


Customising POCI Notifications
------------------------------

The OCI Notification Service can only send plain text emails. The format of the 'Scheduling Events' notification can be customised by Pebble IT, the 'POCI Warnings' and 'POCI Errors' notifications cannot be altered. To customise the appearance of the notification, perform the following steps:

1. Agree with internal stakeholders what the format and frequency of the Scheduling Events notification
2. Obtain an existing Scheduling Event notification and document the changes required -- this can be done in a Word document or Powerpoint presentation
3. Send to POCI Support to request an update to the notification. Refer to the :doc:`Support <ug-support>` section for additional details on obtaining support and assistance for POCI.

Pebble IT will perform the necessary alterations to meet the changes requested once all aspects of the change are agreed.

The notifications allow you to include the following fields in your Scheduling Event notification:

* Resource Details: Type, Display Name, Resource Shape, OCID, Region Name, Compartment
* Event Details: Event Action Date/Time, Event Action, Event Actioned, Tag Source
* Other: Stop On Backup (True/False), Backup Completion Date/Time

Dates can be formatted for any combination of day, month, year, hour and minute.

Notifications that may contain multiple records can be formatted as a table, and the headings of the table can be customised to any suitable value subject to width constraints. Single record notifications can be formatted to be consistently spaced for readability. Both the Subject and Body of the notification can be customised as required.
