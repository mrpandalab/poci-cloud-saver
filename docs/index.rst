.. toctree::
   :maxdepth: 1
   :hidden:

   faq
   Implementation Guide <imp>
   user_guide

.. role:: underline
    :class: underline

################
POCI Cloud Saver
################


About
=====
POCI Cloud Saver (simply referrred to as POCI) is a scheduling tool that allows you to automate the starting and stopping of Oracle Cloud (OCI) Resources. The scheduling information is maintained by your Cloud Administrators as Tags, and POCI caters for both extremely simple and complex scheduling requirements across multiple OCI Resources.

POCI is maintained by `Pebble IT Solutions (Australia) <https://pebbleit.com.au/>`_ and is provided as a fully maintained service by Pebble IT. 


Major Features
==============
POCI has been in use by organizations commencing in 2020 and demonstrates saving thousands of dollars per month. Oracle offers very limited scheduling features of the OCI Resources, and the Open Source solutions that can be found online are basic in capability and unsupported which is an important requirement for commercial and government entities.

POCI now boasts an array of features that result in sophisticated scheduling of resources that realise additional savings, and ease of maintenance for Cloud Administrators. Whilst these documents outline all the capabilities of POCI, the major features that would deliver significant cost savings and ease of maintenance are:

+---------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| Feature                               | Details                                                                                                                      |
|                                       |                                                                                                                              |
+=======================================+==============================================================================================================================+
| Hierarchial Scheduling                | Allows you to enter schedules for all days, or some days of the week and can combine                                         |
+---------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| Compartment Inheritence               | Scheduling can be done at the compartment level or overridden by the resource                                                |
+---------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| Configurable Options                  | A configuration file can customise the behaviour and tag labels during implementation rather than a single capability only   |
+---------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| Multiple Schedules per day            | A resource can start or stop multiple times per day                                                                          |
+---------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| Never Stop or Start                   | A schedule doesn't have to start or stop, ideal when you want manual starts and stops                                        |
+---------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| Stop After Backup Completion          | MySQL and DBCS can be scheduled to stop once the daily backup has completed rather than a fixed time                         |
+---------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| Multiple Regions & Multiple Timezones | Scheduling can be all done in a single timezone or the timezone of the Regions                                               | 
+---------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| Temporary Overrides and Ignores       | The tagging allows Admins to update with temporary changes and ignore periods without impacting the normal scheduling values |
+---------------------------------------+------------------------------------------------------------------------------------------------------------------------------+
| Immediate & Periodic Notifications    | Scheduling activity can be reported immediately or scheduled with multiple events per notification including tag updates     |
+---------------------------------------+------------------------------------------------------------------------------------------------------------------------------+

There are many implementation options that can be chosen during the implementation that are quick and simple to understand, implement and change if necessary. Resource Types can be excluded, regions excluded, regions processed in a particular sequence, and many more.

As POCI caters for sophisticated requirements, simple tag values are updated by POCI into a more comprehensive format so that Administrators can easily understand and expand on the scheduling required for the resource. Freeform tags are maintained with any details of problematic tags and the last scheduling event performed by POCI for the resource.

Read further through the documents to learn about the localisation potential of POCI for your organization and how it may positively impact your OCI costs.

Maintaining Tags
----------------
In its simplest form, POCI allows you to enter a scheduling value like: 

.. image:: main-tags01.png
   :width: 100%

The above tag will instruct POCI to start the database at 6:00am every day and stop it 7:50pm the same day. When POCI first reads this tag, it will update it to a properly formatted value as illustrated below:

.. image:: main-tags02.png
   :width: 100%

This ensures that the tag values are consistent throughout all your OCI Resources. It also ensures that any bad formatting of tags can be notified to Administrators ASAP so that the correct tag value can be entered before scheduling actions are to commence. The tagging can be as complex as required. Below is an example of multiple scheduling requirements required for a database, further explanation follows the picture:

.. image:: main-tags03.png
   :width: 100%

In this instance, the resource schedule has the following requirements:

- don't perform any scheduling until Monday 4-Apr-2022 4am because the DBA team is performing some patching and verification and will manually control the database. POCI will not perform any scheduling instruction until after 4am that day. Note that a comment (starts at the '#') explains to others why this setting is in place so that it is not accidentally removed.
- Until Easter Thursday (8-Apr-2022), the database is to start at 5am on weekdays because there is a special requirement. After that date, this segment will be ignored and instead will be started at 6am. Another comment has been entered explaining this.
- On Monday's, the database will not start until 8am, this overrides the 'weekday' setting on Monday's only. The database runs continuously until 5:45pm and then stops -- it does not wait for backup
- On Weekdays (excluding Monday's), the database will stop before Midday and start again after lunch but not stop until Oracle reports the backup being complete.
- On Weekends, the database is never scheduled to start, but if it is manually started, then it will stop after backup. If however, the Cloud Admins cancelled the backup or started the database after the backup was scheduled, it will stop the database if is running at 10:50pm, this is like a 'catch-all' to ensure the database does not run overnight and incur unnecessary cost.

This is just an example of how complex requirements can be simply met, and yet still be understandable to Cloud Administrators and can easily cater for exceptions and unplanned events. Through localisation, POCI can cater for other languages, different weekdays and week lengths (3 day weekend, 4 weekdays, etc...) as well as complex Timezone requirements to ensure that maintenance of scheduling details are manageable and easily understood.


POCI Documents
--------------
The following documents are available to you to help understand and work with POCI:

         - The :doc:`faq` section provides insights on the uses and capabilities of POCI
         - The :doc:`imp` section contains information on the procedures that are performed to implement POCI and what decisions can be made to realise particular options and outcomes
         - The :doc:`user_guide` section contains information on how Cloud Administrators can use POCI to schedule OCI resources


Inquiries & Support
-------------------
Any inquiries about obtaining POCI or questions regarding its capabilities should be sent to poci@pebbleit.com.au 


Documentation License
---------------------
POCI Documentation Copyright (C) 2022  Pebble IT Solutioms Pty Ltd, Australia.
This documentation comes with ABSOLUTELY NO WARRANTY.

For more information on this, and how to apply and follow the GNU GPL, see
https://www.gnu.org/licenses/

