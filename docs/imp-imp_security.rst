5. Security Rule Design
=======================

POCI requires the implementation of a Security Policy that grants the POCI Server the rights to schedule the resources within the Tenancy. By default, we recommend that the Policy allows POCI to schedule all Resources in all of your Tenancy Regions. However, if required, it is possible to:

* Restrict what Regions that POCI is authorised to schedule resources within; and
* Restrict what Resource Types that POCI is authorised to schedule.

If such restrictions are implemented at the Policy level, then the equivalent restrictions should also be implemented within the configuration of POCI; otherwise authorization errors will be constantly reported and OCI CloudGuard and other Security monitoring solutions may detect attempted unauthorized attempts to perform actions against the OCI Resources.

During the implementation, this will be discussed and naming conventions required will be agreed and who will create the policy will be decided.
