##########################
Frequently Asked Questions
##########################

If you have a question regarding POCI and it is not answered within this documentation, please contact us at poci@pebbleit.com.au 



Doesn't OCI have its own scheduling capabilities ?
   There is minimal capability within OCI for scheduling resources. Oracle updated the ability to schedule  'Autonomous Databases' but only allows a single start and stop per day with little intelligence. The same with Instance Pools which also have very basic scheduling capabilities.



How are OCI Resources billed and how does POCI save costs ?
   All Resource Types that POCI can schedule are billed by the Hour using the UTC Timezone. If the Resource is running at any time during the hour according to the UTC timezone, then its hourly cost will be incurred. Therefore if you operate on a timezone that is not an integer difference to UTC, then you will need to be careful if scheduling resources to start at the start of your local timezone as this may represent 30 minutes past the start of the UTC timezone and therefore you will pay for the full hour even though you only use the resource for 30 minutes.

   For example, the 'Asia/Kolkata' timezone is +5:30 from UTC, therefore 7:00am in Asia/Kolkata is 1:30am UTC, so any resource that starts at that time will already be halfway through the UTC billable hour. Therefore, any resource scheduling in that timezone (for example) would be better served starting at 6:30am and stopping before 30 minutes past the hour (e.g. 8:25 pm).

   POCI saves costs by automating the starting and stopping of resources. Our experience is that the majority of systems that are operational have non-production systems that can be run 14 hours per day for 5 days per week which represents a 58% saving of running that same resource 24x7. 



What Resource Types does POCI Start and Stop ?
   Currently, Version 2 of POCI can start and stop the following resource types:

   * Instances
   * Oracle Database Services on Bare Metal and Virtual Machine
   * Autonomous Databases - OLTP, OLAP & JSON databases
   * MySQL Databases including Heatwave

   We are working on including many more resource types in future releases. If you have a need for a particular resource type, please reach out to us as we may have already catered for that resource type in the current version but not yet documented. Future resource types we are targeting include:

   * Analytics Instances
   * Golden Gate Deployments
   * Instance Pools
   * Integration Instances
   * Load Balancers
   * Digital Assistant
   *  Exadata VM Clusters



Does POCI cater for Oracle Dedicated Regions and Government Regions ?
   Yes it does. POCI is designed to cater for additional OCI Regions that are not part of the public set of OCI Regions. It is likely that a second POCI installation would be required for any use of Oracle Public Cloud Regions in addition to the client-specific Region.



Does POCI work on Oracle Cloud at Customer Installations
   We believe so, but we are still in process of arranging suitable testing. Any scheduling of resources that consume an hourly cost per hour will benefit by the use of POCI. 

   For POCI to operate, your installation will have to include the "Oracle Cloud at Customer Control Plane". This is a standalone offering from Oracle to manage Cloud At Customer deployments, and can include other customer cloud machines including 'Oracle Cloud at Customer', 'Exadata Cloud Machine' (ExaCC), 'Bigdata Cloud at Customer' (BDCC), and other similar solutions that Oracle may make available in the future.

   The control plane consists of the required hardware and Oracle cloud services, including services dashboards and metering and monitoring services. Without the control plane, POCI will not be able to operate. Further details of the Oracle Cloud Control Plane can be found at https://docs.oracle.com/en/cloud/cloud-at-customer/occ-deploy/subscription-models.html



We have subscribed to many regions, can we restrict POCI to some regions only ?
   Yes you can. You can choose to not enter Schedule Tags in those Regions. Alternatively, if that is too difficult to control, you can exclude the Region within the POCI configuration. You can also configure what order Regions are checked/processed by POCI.



Do all tag values have to be in UTC Timezone ?
   No, tag values can be in any timezone you require. Whilst OCI operates on UTC, POCI configuration allows you to nominate a timezone per OCI Region. Any 'canonical timezone' can be used. Refer to https://en.wikipedia.org/wiki/List_of_tz_database_time_zones for a list of eligible timezones.

   Organizations usually elect to have to Schedule Tag values entered in the local timezone of the OCI Region.



What time interval should I start resources after I start a database ?
   Our experience is that the database start time is relatively quick but the database listeners may take up to 12 minutes before they allow connections from other services. Our default recommendation is to allow a 15 minute interval between the start of the database and the start of any service that will error if it cannot immediately connect to the database. Any service that re-tries until success is achieved should be started 5 minutes after the database is started.

   *Note that Pebble IT can implement a change to the database that ensures the database listeners are active sooner than otherwise, this has allowed other customers to have 5 minute intervals between the start of the database and the connecting service. Contact us for further details.*



How does POCI work ?
   POCI operates on a 'Free Tier' server within your home region. It uses policy-driven security to internally access the Oracle Cloud Control Plane to query, start and stop resources. POCI checks every 5 minutes for any scheduling actions to be performed. This all operates internally to your OCI tenancy, therefore there are no external firewall ports that need to be secured or monitored and is a highly secure approach.



Can I have Schedule Tags in different OCI Regions reflect different Timezones ?
   Yes you can. You may nominate the timezone for each OCI Region. Note that timezones that have Daylight Savings are also compatible.


Do ALL Resources have to be tagged ?
   No, only resources that require scheduling by POCI are to have a tag. Alternatively, you can tag the resource's Compartment and all eligible resources within that Compartment can inherit those tag values if certain implementation options are performed. Note that resources that reside in the root compartment can also inherit tag values from the Root Compartment -- which is also the Tenancy. The inheritance is only 1-level deep, child/sub-Compartments do not inherit tag values from the parent Compartment.



What if I want to postpone scheduling for a Resource ? For example, I might want to cancel's tonight stopping of an instance or database ?
   You have the ability to suspend scheduling of a resource or all resources of a compartment or all resources altogether, or alternatively, to only suspend either start or stop events.

   *An example:* A project wants access to the system from 10am - 4pm Saturday & Sunday, normally the project systems stop at 8pm weekdays. It is decided to allow the systems to run continuously all weekend. The Cloud Administrator then enters an 'Ignore-Until' value until the Monday morning. All scheduling of Friday night and onwards are ignored until Monday, and when POCI finds that the ignore instruction is historical, it deletes the instruction and resumes normal operations -- therefore the Cloud Administrator never needs to return and 'clean-up' the revised tag settings.



What if I want to postpone ALL scheduling temporarily ?
   POCI also has the ability to suspend all scheduling by updates to the Tags on the POCI Server instance resource itself. You cannot schedule the POCI Server as it is excluded from all scheduling activity, but its Tag values are used as a source of Tenancy-wide suspension of services.



How can I communicate to other Administrators why a particular Tag value has been set ?
   POCI allows comments -- any text after "#" will be ignored and not treated as a scheduling instruction. This allows you to have the settings explained to others so that they will understand why particular settings have been made and are less likely to change them.

   At one of the POCI sites, all Production instances are tagged with ``run:[NEVER-NEVER] # This is a Production Resource, it is not subject to scheduling`` which is an instruction to state *NEVER* start a resource and *NEVER* stop a resource, and a comment that explains why.



Does POCI send notifications of schedule activity or errors ?
   Notifications can be configured to be sent immediately based on the event type, or on a periodic basis. For example, 'bad tag values' detected might result in an IMMEDIATE sending of a notification, but start/stop events are only sent Mon-Fri at 8:30am to show the prior evenings shutdowns and the mornings startups all in a single notification. This is all performed through the OCI Notification Service and Cloud Administrators can maintain who subscribes to the OCI Notification Topics and is not controlled by POCI.



Is POCI secure ?
   Yes - POCI operates internal to the OCI Tenancy. Management of POCI is performed by SSH. POCI does not include any credentials that could be used by an outside party to gain access to your OCI Tenancy or its resources.



How significant is the cost saving I can achieve with POCI ?
   A resource operating 24 hours per day will incur costs of 24h x 7d x 52 weeks = 8,736 hours per year. If the resource was $1 per hour, then that resource would cost $8,736 per year. If instead that resource could operate on a restricted schedule, for example from 6:00am - 7:55pm daily and not run on Saturday or Sundays, then this would equate to 14h x 5d x 52 weeks = 3,640 hours per year -- that is a reduction of 58.3%, or $5,096 for the year at $1 per hour. If you have 20 resources that could be scheduled, then this would equate to $101,920 for the year. 

   These are significant savings that are realistic to achieve. Whilst the majority of savings are likely to be achieved with non-Production resources, it is possible that Production systems that do not require 24x7 operations will also deliver savings, and some DevOps processes could result in far more significant savings than 58.3% with test environments that are only used for a small number of hours each day. The cost reductions will be determined by your requirements and OCI footprint.

   Another benefit that does interest the organizations that use POCI is that systems that are not running do not represent a security risk, the less time that these systems are available, the less time that they can be subject to compromise. Whilst not a cost saving itself, it is another benefit that is worth considering. 



We are not based in Australia, can we use POCI ?
   Yes, we can implement and maintain POCI anywhere in the world. The only challenge that we may face is timezone differences for online meetings, and language barriers in performing business discussions and English language documentation. Note that POCI is an English language based solution, it can cater for localised Schedule Tag values using english characters left-to-right, it cannot cater for non-English values at this stage.



What if I have other Resource Types to be scheduled ?
   Contact us to discuss your requirements and we will investigate what is possible. If your resource can be started or stopped from the OCI Control Panel and allows the maintenance of Tags, then POCI will be able to automate the scheduling of this resource for you.



Can POCI cater for scheduling hundreds of resources ?
   Yes it can. Current use of POCI reveals that scheduling of resources can occur in a small number of seconds, there should be no restriction in POCI processing hundreds of resources. Alternatively, POCI can be configured to process a subset of resources (resource types, compartments and regions -- or combinations thereof), and multiple POCI Servers are deployed to operate in parallel. This capability would result in POCI being able to schedule tens of thousands of resources.



How will I know if POCI started or stopped a resource ?
   POCI will update a freeform tag on the resource itself with information regarding the action performed by POCI. In addition, the scheduling action is reported by notifications that are configurable to be individual or grouped and are sent through the Oracle Notification Service which allows an unlimited number of people to subscribe to receive the notifications.



How is POCI implemented ?
   POCI is implemented by Pebble IT remotely. The implementation consists of a small number of workshops to introduce the client team to the POCI concepts and options available, and documentation is distributed to capture the configuration options decided. We work in conjunction with your Cloud Team to establish the installation and configuration of POCI. 

   Typically, an implementation takes 3 to 10 days from inception to POCI operating depending upon the availability of the client resources and the complexity of the requirements.



Can I use my service provider to implement POCI and manage POCI for us ?
   At this stage only Pebble IT can implement and manage POCI. However, we have experience in working closely with other service providers and we would engage with them and establish the best approach and outcomes for the client.



If others cannot implement POCI, why is there an Implementation document published ?
   The Implementation document is there to inform you what is required to implement POCI, and gives details on what options can be taken in implementing POCI. The document is published publicly to demonstrate the simplicity of implementing POCI and its capability.



How is POCI licensed ?
   POCI is licensed via a subscription. The subscription terms are 1 or 3 Years. There are no restrictions on the number of resources or regions within the subscription, POCI can be used as little or as much as you require. Contact us for further details.



What occurs if the Subscription is not renewed ?
   If at the end of the Subscription Term you do not renew, arrangements will be made to uninstall POCI. If that is not possible, then there is a grace period where POCI will continue to work normal, then after will continue to operate with 'nag' messages reminding Administrators that POCI has expired before ignoring all scheduling actions.



Can I buy POCI and implement it myself ?
  POCI is only available via Subscription and can only be implemented and managed by Pebble IT.



What if the POCI Server crashes, will all scheduling activity cease ?
  Whilst the POCI Server is unavailable, no scheduling activities can occur. However, upon reboot of the POCI Server, POCI will automatically run and scheduling will commence.



Can we Monitor the POCI Server ?
   Yes you can. We will need to discuss your requirements to perform the monitoring.



How is POCI and the POCI Server Maintained ?
   POCI performs security updates on a weekly basis automatically. POCI can also notify Pebble IT of any issues it is experiencing, and we would resolve any issues as soon as we are aware of what is occurring. Periodically we will update the POCI software to the latest stable version and perform any other operating system maintenance required to ensure the system is current and secure.

