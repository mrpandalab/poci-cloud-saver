Getting Support
===============

All support requests are to be made to poci.support@pebbleit.com.au

.. important::
   Support requests can be for 'How-To' requests as well as technical assistance requests.

   To assist with any technical assistance request, please export the POCI Error & POCI Scheduling Logs from OCI Logging for the 24 hours prior to any issue being encountered and zip to ensure that the attachment is less than 10MB in size.

   Note that POCI Support occurs during business days, 9am - 5pm in AEST, Sydney Australia. This is UTC + 10 hours except during Daylight Savings, which is UTC + 11 hours.


All POCI Support is via email only.
