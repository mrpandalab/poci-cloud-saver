4. The POCI User
================

The POCI User is the user configured within your OCI instance that has the permissions assigned to them to authorize POCI to perform its scheduling operations. This 'user' can be from your organization or from Pebble IT. This user will be granted special permissions and if inactivated due to leaving the organization, then POCI will lose its authorization to perform work.

It is recommended that Pebble IT is an authorized user and this is managed by the organization. In addition to the user, the following artefacts are required to be established within your OCI Tenancy:

* A compartment for the POCI artefacts in OCI -- this can be an existing compartment or preferably, a compartment dedicated to POCI (eg 'POCI' or 'POCI SCHEDULING')
* An instance that is referred to as the POCI Server, there is further details on this later in the Implementation Guide
* A Dynamic Group that includes the POCI Server
* A Security Policy that authorizes the Dynamic Group to perform scheduling actions within your Tenancy
* A Log Agent for 'API Only' usage unless a similar agent already exists
* Two Logs for capturing the POCI output that is viewable within the OCI Control Panel


Details will be shared during the implementation and names of the above artefacts can be agreed to comply with any conventions required.
