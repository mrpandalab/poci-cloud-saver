#########################
POCI User Guide
#########################

The User Guide is a document to be used by any personnel who are tasked with maintaining the scheduling details on the OCI resources. This document will provide the reader with the ability to perform all creating, maintaining and removing tag details to permit POCI to perform its scheduling and notification functions.

   **User Guide Contents**

.. toctree::
   :maxdepth: 2

   ug-tags
   ug-server
   ug-notify
   ug-logs
   ug-advanced
   ug-support

