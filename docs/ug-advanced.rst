Advanced Usage
==================


* Distributing new Tag definitions
* setting compartment tag restrictions for resource types
* setting stop type for MySQL databases
* requesting a tag report
* setting runtime parameters for POCI (& overrides ?)
* Multiple POCI Servers
